package ntnu.idi.stud;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ntnu.idi.stud.view.Home;

import java.util.Objects;

public class Main extends Application {
    Home views = new Home();
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        AnchorPane homeRoot = views.homeRoot();
        Scene homeScene = new Scene(homeRoot);


        stage.setTitle("idatt2003 programmering 2 - arbeidskrav 4");

        String css =
                Objects.requireNonNull(
                        this.getClass().
                                getResource("style/style.css")
                ).toExternalForm();

        homeScene.getStylesheets().add(css);

        stage.setScene(homeScene);
        stage.setMaximized(true);
        stage.show();
    }
}