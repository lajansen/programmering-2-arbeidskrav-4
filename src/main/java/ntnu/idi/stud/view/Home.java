package ntnu.idi.stud.view;

import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import ntnu.idi.stud.controller.Controller;

public class Home {

    Controller controller = new Controller();

    int amountOfCards = 0;

    public AnchorPane homeRoot() {

        Text title = createText("title", "Kortspill");

        Text underTitle = createText("under-title", "idatt2003 programmering 2 - arbeidskrav 4");

        Text currentCardsText = createText("current-cards", "CURRENT CARDS");

        Text sumOfFacesLabel = createText("sum-of-faces-label", "Sum of the faces: ");

        Text sumOfFacesValue = createText("sum-of-faces-value", "SUM OF FACES");

        Text cardsOfHeartsLabel = createText("cards-of-hearts-label", "Cards of hearts: ");

        Text cardsOfHeartsValue = createText("cards-of-hearts-value", "CARDS OF HEARTS");

        Text flushLabel = createText("flush-label", "Flush?: ");

        Text flushValue = createText("flush-value", "Y/N");

        Text queenOfSpadesLabel = createText("queen-of-spades-label", "Queen of spades?: ");

        Text queenOfSpadesValue = createText("queen-of-spades-value", "Y/N");

        Text amountOfCardsValue = createText("amount-of-cards-value", "0");

        Button amountOfCardsMinusButton = createButton("amount-of-cards-minus-button", "-");
        amountOfCardsMinusButton.setOnAction(e -> {
            if (amountOfCards > 0) {
                amountOfCards--;
            }
            amountOfCardsValue.setText(amountOfCards + "");
        });

        Button amountOfCardsPlusButton = createButton("amount-of-cards-plus-button", "+");
        amountOfCardsPlusButton.setOnAction(e -> {
            if (amountOfCards < 52) {
                amountOfCards++;
            }
            amountOfCardsValue.setText(amountOfCards + "");
        });

        Button dealHandButton = createButton("deal-hand-button", "Deal hand");
        dealHandButton.setOnAction(e -> {
            controller.dealHand(amountOfCards);
        });

        Button checkHandButton = createButton("check-hand-button", "Check hand");
        checkHandButton.setOnAction(e -> {
            currentCardsText.setText(controller.getCurrentHandAsString());
            sumOfFacesValue.setText(controller.getSumOfFaces() + "");
            cardsOfHeartsValue.setText(controller.getCurrentCardsOfHeartsAsString());

            if (controller.isFlushInCurrentCards()) {
                flushValue.setText("Sí");
            } else {
                flushValue.setText("Nein");
            }

            if (controller.isQueenOfSpadesInCurrentCards()) {
                queenOfSpadesValue.setText("Sí");
            } else {
                queenOfSpadesValue.setText("Nein");
            }
        });



        AnchorPane root = new AnchorPane();

        VBox masterVBox = new VBox();
        masterVBox.setId("master-v-box");

        masterVBox.getChildren().add(
                title
        );

        masterVBox.getChildren().add(
                underTitle
        );

        HBox currentDealCheckCardsHBox = new HBox();
        currentDealCheckCardsHBox.setId("current-deal-check-cards-h-box");

        VBox currentCardsVBox = new VBox();
        currentCardsVBox.setId("current-cards-v-box");

        currentCardsVBox.getChildren().add(
                currentCardsText
        );

        currentDealCheckCardsHBox.getChildren().add(
                currentCardsVBox
        );

        VBox dealCheckCardsVBox = new VBox();
        dealCheckCardsVBox.setId("deal-check-cards-v-box");

        HBox amountOfCardsHBox = new HBox();
        amountOfCardsHBox.setId("amount-of-cards-h-box");

        amountOfCardsHBox.getChildren().add(
                amountOfCardsMinusButton
        );

        amountOfCardsHBox.getChildren().add(
                amountOfCardsValue
        );

        amountOfCardsHBox.getChildren().add(
                amountOfCardsPlusButton
        );

        dealCheckCardsVBox.getChildren().add(
                amountOfCardsHBox
        );

        dealCheckCardsVBox.getChildren().add(
                dealHandButton
        );

        dealCheckCardsVBox.getChildren().add(
                checkHandButton
        );

        currentDealCheckCardsHBox.getChildren().add(
                dealCheckCardsVBox
        );


        masterVBox.getChildren().add(
                currentDealCheckCardsHBox
        );

        HBox sumOfFacesHBox = new HBox();
        sumOfFacesHBox.setId("sum-of-faces-h-box");

        sumOfFacesHBox.getChildren().add(
                sumOfFacesLabel
        );

        sumOfFacesHBox.getChildren().add(
                sumOfFacesValue
        );

        masterVBox.getChildren().add(
                sumOfFacesHBox
        );

        HBox cardsOfHearts = new HBox();
        cardsOfHearts.setId("cards-of-hearts");

        cardsOfHearts.getChildren().add(
                cardsOfHeartsLabel
        );

        cardsOfHearts.getChildren().add(
                cardsOfHeartsValue
        );

        masterVBox.getChildren().add(
                cardsOfHearts
        );

        HBox flushHBox = new HBox();

        flushHBox.getChildren().add(
                flushLabel
        );

        flushHBox.getChildren().add(
                flushValue
        );

        masterVBox.getChildren().add(flushHBox);

        HBox queenOfSpadesHBox = new HBox();

        queenOfSpadesHBox.getChildren().add(
                queenOfSpadesLabel
        );

        queenOfSpadesHBox.getChildren().add(
                queenOfSpadesValue
        );

        masterVBox.getChildren().add(
                queenOfSpadesHBox
        );

        root.getChildren().add(masterVBox);

        return root;
    }

    private Button createButton(String id, String label) {
        Button button = new Button(label);
        button.setId(id);
        return button;
    }

    private Text createText(String id, String content) {
        Text text = new Text(content);
        text.setId(id);
        return text;
    }

}
