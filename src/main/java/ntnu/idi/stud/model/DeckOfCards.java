package ntnu.idi.stud.model;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

public class DeckOfCards {
    private final char[] suit = {'S', 'H', 'D', 'C'};

    private final Random random = new Random();
    private final ArrayList<PlayingCard> deckOfCards = new ArrayList<>();

    public DeckOfCards() {
        setDeckOfCards();
    }

    private void setDeckOfCards() {
        for (char c : suit) {
            for (int j = 0; j < 13; j++) {
                deckOfCards.add(
                        new PlayingCard(c, j + 1)
                );
            }
        }

    }

    public Collection<PlayingCard> dealHand(int n) throws IllegalArgumentException {
        if (n < 0 || n > deckOfCards.size()) {
            throw new IllegalArgumentException("Amount of cards n, is out of bounds (1 - 52)");
        }

        ArrayList<PlayingCard> hand = new ArrayList<>();
        ArrayList<PlayingCard> allCardsLeft = new ArrayList<>(deckOfCards);

        for (int i = 0; i < n; i++) {
            int randomIndex = random.nextInt(
                    allCardsLeft.size()
            );

            hand.add(
                    allCardsLeft.get(randomIndex)
            );

            allCardsLeft.remove(randomIndex);
        }

        return hand;
    }


}
