package ntnu.idi.stud.controller;

import ntnu.idi.stud.model.DeckOfCards;
import ntnu.idi.stud.model.PlayingCard;

import java.util.ArrayList;
import java.util.Collection;

public class Controller {
    private final DeckOfCards deckOfCards = new DeckOfCards();

    private Collection<PlayingCard> currentHand;

    public Controller() {
        currentHand = new ArrayList<>();
    }

    public void dealHand(int amountOfCards) {
        try {
            currentHand = deckOfCards.dealHand(amountOfCards);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public String getCurrentHandAsString() {
        return getCollectionAsString(currentHand);
    }

    public String getCurrentCardsOfHeartsAsString() {
        return getCollectionAsString(
                currentHand
                        .stream()
                        .filter(playingCard -> playingCard.getSuit() == 'H')
                        .toList()
        );
    }

    public int getSumOfFaces() {
        return currentHand
                .stream()
                .mapToInt(PlayingCard::getFace)
                .sum();
    }

    public boolean isFlushInCurrentCards() {
        char[] suit = {'H', 'D', 'C', 'S'};

        for (char s : suit) {
            int countOfSameCard =
                    currentHand
                            .stream()
                            .filter(
                                    playingCard -> playingCard.getSuit() == s
                            )
                            .toList()
                            .size();

            if (countOfSameCard >= 5) {
                return true;
            }
        }

        return false;
    }

    public boolean isQueenOfSpadesInCurrentCards() {
        return currentHand
                .stream()
                .anyMatch(
                        playingCard -> playingCard.getSuit() == 'S'
                                && playingCard.getFace() == 12
                );
    }

    private String getCollectionAsString(Collection<PlayingCard> collection) {
        StringBuilder stringBuilder = new StringBuilder();

        if (collection.isEmpty()) {
            return "Empty hand";
        }

        collection.forEach(playingCard -> {
            stringBuilder.append(playingCard.getAsString()).append(" ");
        });

        return stringBuilder.toString();
    }
}
