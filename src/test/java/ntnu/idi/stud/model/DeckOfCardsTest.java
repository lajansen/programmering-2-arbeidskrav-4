package ntnu.idi.stud.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {

    private DeckOfCards deckOfCards;

    @BeforeEach
    void setUp() {
        deckOfCards = new DeckOfCards();
    }

    @AfterEach
    void tearDown() {
    }

//    @Test
//    void constructorTest() {
//        assertEquals(52, deckOfCards.getDeckOfCards().size());
//        assertEquals(52, deckOfCards.getDeckOfCards().stream().distinct().count());
//
//        char[] suit = {'S', 'H', 'D', 'C'};
//
//        for (int i = 0; i < suit.length; i++) {
//            for (int j = 0; j < 13; j++) {
//                PlayingCard card = new PlayingCard(suit[i], j + 1);
//                assertEquals(card.getFace(), deckOfCards.getDeckOfCards());
//            }
//        }
//    }

    @Test
    void dealHandTest() {
        for (int i = 0; i < 52; i++) {
            assertEquals(
                    i + 1,
                    deckOfCards.dealHand(i + 1).stream().distinct().count()
            );
        }

        assertThrows(
                IllegalArgumentException.class,
                () -> deckOfCards.dealHand(-1)
        );

        assertThrows(
                IllegalArgumentException.class,
                () -> deckOfCards.dealHand(53)
        );
    }
}